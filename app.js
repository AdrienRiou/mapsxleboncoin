const express = require('express');
const path = require('path');
const cors = require('cors');
const axios = require('axios');


const app = express();
const port = process.env.PORT || 3000;

// Enable CORS for all routes
app.use(cors());

// Serve static files from the 'scripts' directory
app.use('/scripts', express.static('scripts'));

// Serve static files from the 'public' directory
app.use(express.static(path.join(__dirname, 'public')));

// Handle requests to the root URL
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Proxy route for Nominatim search
app.get('/nominatim-search', async (req, res) => {
    const { format, q } = req.query;
    try {
        const response = await axios.get(`https://nominatim.openstreetmap.org/search?format=${format}&q=${encodeURIComponent(q)}`);
        res.json(response.data);
    } catch (error) {
        console.error('Error proxying Nominatim search:', error.response ? error.response.data : error.message);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

// Proxy route for Nominatim reverse geocoding
app.get('/nominatim-reverse', async (req, res) => {
    const { lat, lon, format } = req.query;
    try {
        const response = await axios.get(`https://nominatim.openstreetmap.org/reverse?lat=${lat}&lon=${lon}&format=${format}`);
        res.json(response.data);
    } catch (error) {
        console.error('Error proxying Nominatim reverse geocoding:', error.response ? error.response.data : error.message);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

// Proxy route for Overpass API
app.get('/overpass-api', async (req, res) => {
    const { data } = req.query;
    try {
        const response = await axios.get(`https://overpass-api.de/api/interpreter?data=${encodeURIComponent(data)}`);
        res.json(response.data);
    } catch (error) {
        console.error('Error proxying Overpass API:', error.response ? error.response.data : error.message);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

// Proxy route for Overpass API
app.get('/mapbox-api', async (req, res) => {
    try {
        const response = await axios.get(`https://api.mapbox.com/directions/v5/mapbox/driving/2.3483915%2C48.8534951%3B-4.486009%2C48.390528?alternatives=true&geometries=geojson&language=en&overview=full&steps=true&access_token=pk.eyJ1IjoiYWRyaWVucmlvdSIsImEiOiJjbHBoZDQzMTkwN2FyMmtudnllNG5wM2UyIn0.DGO9T4iValxGnOXNC33D5g`);
        res.json(response.data);
    } catch (error) {
        console.error('Error proxying Overpass API:', error.response ? error.response.data : error.message);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});


// Start the server
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
