export async function getCitiesFromCoordinates(coordinates) {
    try {

        // Store unique cities
        const uniqueCities = new Set();

        await Promise.all(coordinates.map(async (coord) => {

            const [lon, lat] = coord;
            const response = await axios.get(`http://localhost:3000/nominatim-reverse?lat=${lat}&lon=${lon}&format=json`);

            const address = response.data.address;

            //Extract the city from the address
            const city = address.city || address.town || address.village || address.hamlet;

            //Add the city to the Set (which automatically ensures uniqueness)
            uniqueCities.add(city);

        }));

        //return Array.from(uniqueCities);
        return uniqueCities;

    } catch (error) {
        console.error('Error:', error.response ? error.response.data : error.message);
        throw error;
    }
}



const mapboxApiKey = 'pk.eyJ1IjoiYWRyaWVucmlvdSIsImEiOiJjbHBoZDQzMTkwN2FyMmtudnllNG5wM2UyIn0.DGO9T4iValxGnOXNC33D5g'; 

export async function getRouteCoordinatesBis() {
    try {
        // Step 1: Get the route details using Mapbox Directions API
        /* const directionsResponse = await axios.get('https://api.mapbox.com/directions/v5/mapbox/driving', {
            params: {
                waypoints: [
                    { coordinates: [2.3522, 48.8566] }, // Paris coordinates
                    { coordinates: [-4.4860, 48.3904] },  // Brest coordinates
                ],
                access_token: mapboxApiKey,
                steps: true, // Include detailed step-by-step instructions
            },
        }); */

        const directionsResponse = await axios.get('http://localhost:3000/mapbox-api/');


        const route = directionsResponse.data.routes[0];

        //console.log('Route : ', route.geometry.coordinates);


        return route.geometry.coordinates;
        

        //console.log(simplifiedCoordinates);
    } catch (error) {
        console.error('Error fetching route coordinates:', error.message);
    }
}