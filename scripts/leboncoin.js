// Functions for generating Leboncoin URLs


function generateLeboncoinURLs(routeCoordinates, object, minPrice, maxPrice) {
    const baseURL = "https://www.leboncoin.fr/recherche";

    // Array to store generated Leboncoin URLs
    const leboncoinURLs = [];

    // Iterate through the route coordinates
    for (let i = 0; i < routeCoordinates.length - 1; i++) {
        // Extract start and end coordinates for the current segment
        const start = routeCoordinates[i];
        const end = routeCoordinates[i + 1];

        // Construct Leboncoin URL for the current segment
        const leboncoinURL = `${baseURL}?text=${object}&locations=${start}.${end}&price=min-${minPrice}`;

        // Add the URL to the array
        leboncoinURLs.push(leboncoinURL);
    }

    // Return the array of Leboncoin URLs
    return leboncoinURLs;
}