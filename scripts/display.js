// Functions for displaying data on the HTML page

export function displayUrlsInTable(urls, cities, object) {
    const urlTable = document.getElementById('url-table');
    // Clear previous table rows
    urlTable.innerHTML = '';

    // Add new rows to the table
    for (let i = 0; i < urls.length; i++) {
        const city = cities[i];
        const url = urls[i];

        const row = urlTable.insertRow();
        const cellCity = row.insertCell(0);
        const cellUrl = row.insertCell(1);

        // Customize styling for the first and last cities
        const isFirstCity = i === 0;
        const isLastCity = i === cities.length - 1;

        const fontSize = isFirstCity || isLastCity ? '20px' : '16px';
        const fontColor = isFirstCity || isLastCity ? 'red' : 'black';
        const fontWeight = isFirstCity || isLastCity ? 'bold' : 'normal';

        // Apply styling to the city name
        const styledCity = `<span style="font-size: ${fontSize}; color: ${fontColor}; font-weight: ${fontWeight};margin-right: 30px">${city}</span>`;

        const urlFontSize = '16px'; // Set the desired font size for URLs

        // Use the object name from the form as the label
        const label = `${object} disponibles dans les environs de ${city}`;


        cellCity.innerHTML = styledCity;
        cellUrl.innerHTML = `<a href="${url}" target="_blank" style="font-size: ${urlFontSize};">${label}</a>`;
    }

    // Show the URL table
    document.getElementById('url-table-container').style.display = 'block';
}
