export function getFilteredCoordinates(coordinates) {    

    const result = [];
    //const distanceInterval = 5; // Distance interval in kilometers
    let accumulatedDistance = 0;

    for (let i = 0; i < coordinates.length - 1; i++) {
        const coord1 = coordinates[i];
        const coord2 = coordinates[i + 1];
        const segmentDistance = calculateDistance(coord1, coord2);

        // Check if adding coordinates is needed for the current segment
       /*  while (accumulatedDistance + segmentDistance >= distanceInterval) {
            const remainingDistance = distanceInterval - accumulatedDistance;
            const ratio = remainingDistance / segmentDistance;

            // Calculate coordinates along the segment
            const newCoord = [
                coord1[0] + ratio * (coord2[0] - coord1[0]),
                coord1[1] + ratio * (coord2[1] - coord1[1])
            ];

            result.push(newCoord);
            accumulatedDistance = 0; // Reset accumulated distance
        } */

        //accumulatedDistance += segmentDistance;
    }

    // Add the last coordinate from the original route
    result.push(coordinates[coordinates.length - 1]);

    return result;
}

export function calculateDistance(coord1, coord2) {
    const [lat1, lon1] = coord1;
    const [lat2, lon2] = coord2;

    // Convert latitude and longitude from degrees to radians
    const toRadians = angle => angle * (Math.PI / 180);
    const dLat = toRadians(lat2 - lat1);
    const dLon = toRadians(lon2 - lon1);

    // Haversine formula
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
              Math.cos(toRadians(lat1)) * Math.cos(toRadians(lat2)) *
              Math.sin(dLon / 2) * Math.sin(dLon / 2);

    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    // Earth radius in kilometers (adjust as needed)
    const R = 6371;

    // Calculate the distance
    const distance = R * c;

    return distance;
}


export function calculateTotalDistance(coordinates) {
    let totalDistance = 0;

    for (let i = 0; i < coordinates.length - 1; i++) {
        const distance = calculateDistance(coordinates[i], coordinates[i+1]);

        totalDistance += distance;
    }

    return totalDistance;
}


export function keepEveryNthCoordinate(cordinates, n) {
    if (n <= 1) {
        // If n is less than or equal to 1, return the original route
        return cordinates;
    }

    const result = [];

    for (let i = 0; i < cordinates.length; i += n) {
        result.push(cordinates[i]);
    }

    return result;
}