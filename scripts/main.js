import { displayUrlsInTable } from './display.js';
import { getCitiesFromCoordinates, getRouteCoordinatesBis } from './api.js';
import { keepEveryNthCoordinate, getFilteredCoordinates } from './utils.js';
import { initRoute } from './map.js';

// Initialization logic, event listeners, and main application code



async function planTravel() {

    try {

        const origin = document.getElementById('departure').value;
        const destination = document.getElementById('arrival').value;
        const object = document.getElementById('object').value;
        const minPrice = document.getElementById('minPrice').value;
        const maxPrice = document.getElementById('maxPrice').value;



        const coordinates = await getRouteCoordinatesBis(origin, destination);

        //getFilteredCoordinates(coordinates);

        const coordinatesBos = keepEveryNthCoordinate(coordinates, 10);

        //const filteredCoordinates = keepEveryNthCoordinate(coordinates, 5000);

        initRoute(coordinates);




        //const citiesList = await getCitiesFromCoordinates(coordinatesBos);

        //console.log('Les villes traversées : ', citiesList);

        // Display the generated URLs on the HTML page
        //displayUrlsInTable(leboncoinURLs, cities);

        // Draw the route on the map
        //drawRealisticRoute(coordinates);

        


        // Combine and return all coordinates
        //return coordinates;

    } catch (error) {
        console.error('Error:', error.response ? error.response.data : error.message);
        throw error;
    }
}


document.getElementById('travelForm').addEventListener('submit', function (event) {
    event.preventDefault();
    planTravel();
});