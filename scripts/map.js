// Leaflet map setup and functions


export function initRoute(coordinates) {

    const franceCenter = [1.8888, 46.6035];
    const initialZoom = 5;

    mapboxgl.accessToken = 'pk.eyJ1IjoiYWRyaWVucmlvdSIsImEiOiJjbHBoZDQzMTkwN2FyMmtudnllNG5wM2UyIn0.DGO9T4iValxGnOXNC33D5g';

    const map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: franceCenter,
        zoom: initialZoom,
    });

    const popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false,
        anchor: 'top-left',
        offset: 15,
    })
        .setLngLat(coordinates[0])
        .setHTML(createPopupHTML())
        .addTo(map);

    map.on('load', () => {
        map.addLayer({
            id: 'route',
            type: 'line',
            source: {
                type: 'geojson',
                data: {
                    type: 'Feature',
                    properties: {},
                    geometry: {
                        type: 'LineString',
                        coordinates: coordinates,
                    },
                },
            },
            layout: {
                'line-join': 'round',
                'line-cap': 'round',
            },
            paint: {
                'line-color': '#888',
                'line-width': 8,
            },
        });
    });
    
    // Function to create HTML content for the popup
    function createPopupHTML() {
        const popupContent = document.createElement('div');
        popupContent.innerHTML = `
            <h3>Route Details</h3>
            <p>Distance: ${calculateDistance(coordinates)} meters</p>
            <p>Duration: ${calculateDuration(coordinates)} seconds</p>
            <button onclick="acceptRoute()">Accepter ce trajet</button>
            <button onclick="closePopup()">Autre trajet</button>
        `;
        return popupContent;
    }

    // Function to calculate distance of the route
    function calculateDistance(coords) {
        // Calculate the total distance using the haversine formula or any other method
        // Replace this with your actual distance calculation logic
        return 0;
    }

    // Function to calculate duration of the route
    function calculateDuration(coords) {
        // Calculate the total duration based on your route details
        // Replace this with your actual duration calculation logic
        return 0;
    }
    
    
    // Function to close the popup
    function closePopup() {
        popup.remove();
    }
    
    // Function to handle accepting the route
    function acceptRoute() {
        // Add your logic for handling the accepted route
        console.log('Route accepted!');
        // For now, simply close the popup
        closePopup();
    }
    
}


function calculateCenter(coordinates) {
    if (coordinates.length === 0) {
        return null; // Return null for an empty array
    }

    // Initialize variables for sum of latitudes and longitudes
    let sumLat = 0;
    let sumLon = 0;

    // Iterate through each coordinate and sum up latitudes and longitudes
    coordinates.forEach(coord => {
        sumLat += coord[0]; // Latitude
        sumLon += coord[1]; // Longitude
    });

    // Calculate average latitudes and longitudes
    const avgLat = sumLat / coordinates.length;
    const avgLon = sumLon / coordinates.length;

    return [avgLat, avgLon];
}