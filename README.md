# MapsxLeboncoin



## Getting started

## 1. Installer Node.js et npm :

Télécharge et installe Node.js à partir du site officiel : Node.js. (C'est du JS backend, tout le projet est basé là dessus).

## 2. Cloner le projet :

Ouvre un terminal (ou l'invite de commande sur Windows) et exécute la commande suivante pour cloner le projet depuis GitLab :

<code>git clone https://gitlab.com/AdrienRiou/mapsxleboncoin.git</code>

--> Ça créera un dossier avec le nom du projet dans ton répertoire actuel.

## 3. Accéder au dossier du projet :

Change ton répertoire de travail vers le dossier du projet avec la commande cd (Change Directory) :

<code>cd mapsxleboncoin</code>

--> Ça va te mettre dans le dossier du projet

## 4. Installer les dépendances :

Exécute la commande suivante pour installer les dépendances du projet. Ceci utilisera le fichier package.json pour récupérer les dépendances nécessaires.

<code>npm install</code>

## 5. Lancer le serveur avec nodemon :

Assure-toi que nodemon est installé de manière globale en utilisant la commande suivante (tu n'as à le faire qu'une fois) :

<code>npm install -g nodemon</code>

Ensuite, pour lancer le serveur avec nodemon, utilise la commande suivante :

<code>nodemon app.js</code>

--> Le serveur devrait se lancer, et tu verras probablement un message dans la console indiquant que le serveur est à l'écoute sur le port 3000.

## 6. Accéder au site dans le navigateur :

Ouvre ton navigateur web et accède à l'URL suivante :

http://localhost:3000

Et voilà ! Tu devrais maintenant voir le projet s'exécuter localement sur ton ordi.